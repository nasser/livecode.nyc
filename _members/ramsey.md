---
name: Ramsey Nasser
image: https://increment.com/art/5/authors/nasser-500-6fd90727.jpeg
link: http://nas.sr/
---

Nerds out over video games and experimental programming languages, and teaches people to do the same at schools around the city.
