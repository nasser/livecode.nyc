---
name: Sarah Groff Hennigh-Palermo
image: https://www.dropbox.com/s/ygdlqrk7zfhsnc7/cartoon_sarah.png
link: http://sarahghp.com/
---

Brooklyn-based artist and programmer. Looking for the answer to _What if Malevich, Lichtenstein, and Martin had a baby and this baby learned to code?_ Graphix with (Codie)[https://twitter.com/hi_codie].
