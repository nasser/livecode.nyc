---
name: Ronan Rice
image: http://ronanrice.com/img/livecode.jpg
link: http://ronanrice.com
---

Harlem-based semiconductor engineer. Lives for music, art and racing.
