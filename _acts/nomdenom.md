---
name: nom de nom
image: https://nickm.com/nom_de_nom_500x500.jpg
link: http://www.pouet.net/user.php?who=56837
---

Two Commodore 64s, a video switch, and some BASIC programming can bring visual poetry alive.
