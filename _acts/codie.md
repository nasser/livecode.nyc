---
name: Codie
image: https://pbs.twimg.com/profile_images/937400350755057664/NJcpaKZ5_400x400.jpg
link: https://www.instagram.com/hi_codie/
---

Trio based in Brooklyn and Virginia. Abstract art, abstract sounds, accumulation. [We made a film once.](https://vimeo.com/282220342)
