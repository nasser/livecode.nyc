---
name: Arcadia
image: http://ludumdare.com/compo/wp-content/uploads/2015/04/lobster1.gif
link: https://arcadia-unity.github.io/
---

The integration of [Clojure](https://clojure.org/) and [Unity 3D](https://unity3d.com/). Turns a world class game engine into a live coding visualist power tool.
