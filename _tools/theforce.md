---
name: The Force
image: http://www.shawnlawson.com/wp-content/uploads/2016/12/mintThumb.png
link: https://github.com/shawnlawson/The_Force
---

Life creates it, makes it grow. Its energy surrounds us and binds us. Luminous beings are we, not this crude matter. You must feel the Force around you; here, between you, me, the tree, the rock, everywhere, yes.

Browser-based GLSL