---
name: The Dark Side
image: http://www.shawnlawson.com/wp-content/uploads/2018/08/darkside.png
link: https://github.com/shawnlawson/TheDarkSide
---

The dark side of the Force is a pathway to many abilities some consider to be unnatural.

Multi-user, telematic, and simultaneous GLSL & [Tidal](https://tidalcycles.org) livecoding with NodeJS. Also includes text edit recording and playback.